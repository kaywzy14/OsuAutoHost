module room;

import config;
import core.time;
import bancho.irc;
import std.algorithm;
import std.conv;
import std.datetime;
import std.math;
import std.random;
import std.range;
import api.map;
import vibe.vibe;
import tinyevent;
import mongoschema;
import db;

enum ApprovalFlags
{
	graveyard = 1 << 0,
	wip = 1 << 1,
	pending = 1 << 2,
	ranked = 1 << 3,
	approved = 1 << 4,
	qualified = 1 << 5,
	loved = 1 << 6,
	all = graveyard | wip | pending | ranked | approved | qualified | loved,
}

enum GenreFlags
{
	unknown = 1 << 0,
	videoGame = 1 << 1,
	anime = 1 << 2,
	rock = 1 << 3,
	pop = 1 << 4,
	other = 1 << 5,
	novelty = 1 << 6,
	hipHop = 1 << 7,
	electronic = 1 << 8,
	all = unknown | videoGame | anime | rock | pop | other | novelty | hipHop | electronic
}

enum LanguageFlags
{
	unknown = 1 << 0,
	other = 1 << 1,
	english = 1 << 2,
	japanese = 1 << 3,
	chinese = 1 << 4,
	instrumental = 1 << 5,
	korean = 1 << 6,
	french = 1 << 7,
	german = 1 << 8,
	swedish = 1 << 9,
	spanish = 1 << 10,
	italian = 1 << 11,
	all = unknown | other | english | japanese | chinese | instrumental | korean
		| french | german | swedish | spanish | italian
}

enum ModeFlags
{
	osu = 1 << 0,
	taiko = 1 << 1,
	ctb = 1 << 2,
	mania = 1 << 3,
	all = osu | taiko | ctb | mania
}

enum MapSelectorType
{
	hostRotator,
	userPlaylist,
	genericPlaylist
}

struct MapWithInfo
{
	string map;
	Mod[] mods;
	string comment;
}

struct AutoHostSettings
{
	enum MapDiffPick
	{
		random,
		lowest,
		highest,
		all
	}

	/// Internal ID to revive a room after it being closed or bot restarting.
	string internalID;
	string titlePrefix;
	string titleSuffix = "Mini Host Rotate";
	bool starsInTitle = true;
	bool enforceTitle = true;
	bool enforcePassword = true;
	ubyte slots = 8;
	string password;
	string[] startInvite;
	/// Inclusive min/max values for the recommended stars rating of a map. A max of 4 includes 4.99* maps. 0 enforces no limit.
	int minStars, maxStars;
	Duration startGameDuration = 3.minutes;
	Duration allReadyStartDuration = 15.seconds;
	Duration selectWarningTimeout = 60.seconds;
	Duration selectIdleChangeTimeout = 30.seconds;
	Duration manualStartDuration = 15.seconds;
	/// Number of maps to look back in playing history to deny. (Max 100)
	int recentlyPlayedLength = 5;

	ApprovalFlags preferredApproval = ApprovalFlags.all;
	GenreFlags preferredGenre = GenreFlags.all;
	LanguageFlags preferredLanguage = LanguageFlags.all;
	ModeFlags preferredMode = ModeFlags.all;
	/// Osu name of the creator (for !temphost permissions and in !info)
	string creator;
	/// Reopen if closed
	bool autoReopen = true;
	MapSelectorType type;

	struct HostRotatorSettings
	{
	}

	HostRotatorSettings hostRotatorSettings;

	struct UserPlaylistSettings
	{
		string[] mappers;
		MapDiffPick mapDiff;
		size_t mapDiffMaxNum = 1;
	}

	UserPlaylistSettings userPlaylistSettings;

	struct GenericPlaylistSettings
	{
		MapWithInfo[] maps;
	}

	GenericPlaylistSettings genericPlaylistSettings;

	string toString() const
	{
		string ret;
		ret = "Auto Host, " ~ minStars.to!string ~ "-";
		if (maxStars)
			ret ~= maxStars.to!string;
		ret ~= "*. Room prefers: map approval: " ~ stringifyBitflags(
				preferredApproval) ~ ", song genre: " ~ stringifyBitflags(preferredGenre) ~ ", song language: " ~ stringifyBitflags(
				preferredLanguage) ~ ", game mode: " ~ stringifyBitflags(
				preferredMode) ~ ". Room created by " ~ creator ~ ".";
		return ret;
	}

	bool inStarRange(double value)
	{
		auto stars = cast(int) floor(value);
		if (minStars && maxStars)
			return stars >= minStars && stars <= maxStars;
		else if (minStars)
			return stars >= minStars;
		else if (maxStars)
			return stars <= maxStars;
		else
			return true;
	}

	bool matchesApproval(Approval value)
	{
		if (preferredApproval == ApprovalFlags.all)
			return true;
		return !!(preferredApproval & (1 << (value - Approval.min)));
	}

	bool matchesGenre(MapGenre value)
	{
		if (preferredGenre == GenreFlags.all)
			return true;
		if (value == MapGenre.any || value == MapGenre.unspecified)
			return !!(preferredGenre & GenreFlags.unknown);
		else if (value <= MapGenre.novelty)
			return !!(preferredGenre & (1 << (value - 1)));
		else
			return !!(preferredGenre & (1 << (value - 2)));
	}

	bool matchesLanguage(MapLanguage value)
	{
		if (preferredLanguage == LanguageFlags.all)
			return true;
		return !!(preferredLanguage & (1 << (value - MapLanguage.min)));
	}

	bool matchesMode(MapMode value)
	{
		if (preferredMode == ModeFlags.all)
			return true;
		return !!(preferredMode & (1 << (value - MapMode.min)));
	}

	/// Returns a percentage how much a map violates the room rules. (0 being not at all, 1 being inacceptable)
	double evaluateMapSkip(ref MapInfo map, out string[] errors)
	{
		int penalties;
		int total;
		if (preferredMode != ModeFlags.all)
		{
			total += 8;
			if (!matchesMode(map.mode))
			{
				penalties += 8;
				errors ~= text("Please pick a ", stringifyBitflags(preferredMode), " map.");
			}
		}
		if (minStars || maxStars)
		{
			total += 6;
			if (!inStarRange(map.difficultyRating))
			{
				if ((minStars && map.difficultyRating < minStars) || (maxStars
						&& map.difficultyRating + 1 > maxStars))
				{
					penalties += 6;
					errors ~= "Map difficulty out of preferred star range.";
				}
				else
				{
					penalties += 4;
					errors ~= "Map difficulty slightly out of preferred star range.";
				}
			}
		}
		if (preferredApproval != ApprovalFlags.all)
		{
			total += 3;
			if (!matchesApproval(map.approved))
			{
				penalties += 3;
				errors ~= text("This map is not ", stringifyBitflags(preferredApproval), ".");
			}
		}
		if (preferredGenre != GenreFlags.all)
		{
			total += 2;
			if (!matchesGenre(map.genre))
			{
				penalties += 2;
				errors ~= text("Map genre does not match ", stringifyBitflags(preferredGenre), ".");
			}
		}
		if (preferredLanguage != LanguageFlags.all)
		{
			total += 2;
			if (!matchesLanguage(map.language))
			{
				penalties += 2;
				errors ~= text("Map language does not match ", stringifyBitflags(preferredGenre), ".");
			}
		}
		return penalties / cast(double) total;
	}
}

struct PersistentAutoHostRoom
{
	@mongoUnique string internalID;
	@mongoUnique string mpID;
	AutoHostSettings settings;
	string[] hostOrder;
	size_t currentHost;

	mixin MongoSchema;
}

string stringifyBitflags(T)(T flags)
{
	if (!flags)
		return "none";
	if (flags == T.all)
		return "all";
	string ret;
	size_t lastComma;
	size_t i = 1;
	while (i && i <= T.max)
	{
		if ((flags & i) != 0)
		{
			if (ret.length)
			{
				lastComma = ret.length;
				ret ~= ", ";
			}
			ret ~= (cast(T) i).to!string;
		}
		i *= 2;
	}
	if (lastComma)
		return ret[0 .. lastComma] ~ " or" ~ ret[lastComma + 1 .. $];
	else
		return ret;
}

void createAutoHostRoom(AutoHostSettings settings)
in
{
	assert(settings.internalID);
}
do
{
	string[] hostOrder;
	size_t setHost;

	string expectedTitle;
	{
		string prefix;
		if (settings.starsInTitle)
		{
			if (settings.minStars && settings.maxStars)
			{
				if (settings.minStars == settings.maxStars)
					prefix = text(settings.minStars, "* ");
				else
					prefix = text(settings.minStars, " - ", settings.maxStars, ".99* ");
			}
			else if (settings.minStars)
				prefix = text(settings.minStars, "*+ ");
			else if (settings.maxStars)
				prefix = text("0-", settings.maxStars, "* ");
		}
		expectedTitle = settings.titlePrefix ~ (settings.starsInTitle ? prefix : "")
			~ settings.titleSuffix;
	}

	OsuRoom room;

	auto existing = PersistentAutoHostRoom.tryFindOne(["internalID" : settings.internalID]);

	BsonObjectID persistID;

	if (existing.isNull)
	{
		return createNewAutoHostRoom(settings, expectedTitle);
	}
	else
	{
		OsuRoom.Settings info;
		try
		{
			room = banchoConnection.fromUnmanaged(existing.mpID);
			info = room.settings;
		}
		catch (Exception e)
		{
			logInfo("Error when loading existing room: %s", e.msg);
			logInfo("Creating new room...");
		}
		if (info == OsuRoom.Settings.init)
		{
			return createNewAutoHostRoom(settings, expectedTitle, existing.bsonID);
		}
		room.sendMessage("Assuming control over this room (Previous instance shut down)");
		MapSelector selector;
		final switch (settings.type)
		{
		case MapSelectorType.hostRotator:
			hostOrder = existing.hostOrder;
			setHost = existing.currentHost;
			string[] existingUsers;
			foreach (i, player; info.players)
			{
				if (player != OsuRoom.Settings.Player.init)
				{
					existingUsers ~= player.name;
					if (!hostOrder.canFind(player.name))
					{
						hostOrder ~= player.name;
					}
				}
			}
			foreach_reverse (i, user; hostOrder)
			{
				if (!existingUsers.canFind(user))
				{
					hostOrder = hostOrder.remove(i);
					if (i >= setHost)
						setHost--;
				}
			}
			if (setHost == size_t(-1))
				setHost = 0;
			selector = new HostRotator(hostOrder, setHost);
			room.sendMessage("Host order: " ~ hostOrder.join(", "));
			break;
		case MapSelectorType.userPlaylist:
			try
			{
				selector = new PlaylistSelector(settings);
			}
			catch (Exception)
			{
				return;
			}
			break;
		case MapSelectorType.genericPlaylist:
			try
			{
				selector = new LitePlaylistSelector(settings);
			}
			catch (Exception)
			{
				return;
			}
			break;
		}
		existing.settings = settings;
		existing.save();
		ManagedRoom managedRoom = new ManagedRoom(room, settings, expectedTitle,
				selector, existing.bsonID);
		if (settings.enforceTitle && expectedTitle.length && info.name.strip != expectedTitle.strip)
			room.sendMessage("Please rename room name to: " ~ expectedTitle);
	}
}

void createNewAutoHostRoom(AutoHostSettings settings, string expectedTitle,
		BsonObjectID existingPersist = BsonObjectID.init)
{
	MapSelector selector;
	final switch (settings.type)
	{
	case MapSelectorType.hostRotator:
		selector = new HostRotator([], 0);
		break;
	case MapSelectorType.userPlaylist:
		try
		{
			selector = new PlaylistSelector(settings);
		}
		catch (Exception e)
		{
			logError("Failed starting room %s (id=%s) with error: %s", expectedTitle, settings.internalID, e);
			return;
		}
		break;
	case MapSelectorType.genericPlaylist:
		try
		{
			selector = new LitePlaylistSelector(settings);
		}
		catch (Exception e)
		{
			logError("Failed starting room %s (id=%s) with error: %s", expectedTitle, settings.internalID, e);
			return;
		}
		break;
	}
	PersistentAutoHostRoom persist;
	if (existingPersist.valid)
		persist.bsonID = existingPersist;
	persist.internalID = settings.internalID;
	auto room = banchoConnection.createRoom(expectedTitle);
	scope (failure)
		room.close();
	persist.mpID = "#mp_" ~ room.room;
	persist.settings = settings;
	persist.save();
	scope (failure)
		persist.remove();
	room.password = settings.password;
	foreach (user; settings.startInvite)
		room.invite(user);
	runTask({ room.size = settings.slots; room.mods = [Mod.FreeMod]; });
	ManagedRoom managedRoom = new ManagedRoom(room, settings, expectedTitle,
			selector, persist.bsonID);
}

auto getNow() @property
{
	return Clock.currTime(UTC());
}

struct NewUserGreeter
{
	string[] newUsers;
	Timer newUserTimer;
	OsuRoom room;

	void load(OsuRoom room)
	{
		newUserTimer = createTimer(&greetNewUsers);
		this.room = room;

		room.onUserJoin = &onUserJoin ~ room.onUserJoin;
		room.onUserLeave = &onUserLeave ~ room.onUserLeave;
	}

	void greetNewUsers() nothrow @trusted
	{
		if (newUsers.length)
		{
			try
			{
				room.sendMessage(
						"Welcome! This room's Game Host is automatically managed by a bot. To find out more, type !info");
				newUsers.length = 0;
			}
			catch (Exception)
			{
			}
		}
	}

	void onUserJoin(string user, ubyte, Team)
	{
		try
		{
			auto obj = GameUser.findByUsername(user);
			if (obj.joins == 0)
			{
				newUsers ~= user;
				newUserTimer.rearm(240.seconds);
			}
			obj.didJoin();
		}
		catch (Exception e)
		{
			logError("Error onUserJoin: %s", e);
			room.sendMessage(user ~ ": I have been awaiting you.");
		}
	}

	void onUserLeave(string user)
	{
		newUsers = newUsers.remove!(a => a == user);
	}
}

struct MapSkipper
{
	double requiredSkips = 1;
	string[] skippers;
	string[] skippedMapIDs;
	Duration currentMapDuration = 5.minutes;
	SysTime startingTime;
	OsuRoom room;
	SysTime lastInfo;
	bool isRepeated;
	bool didSkip;
	ManagedRoom manager;

	void load(OsuRoom room, ManagedRoom manager)
	{
		this.room = room;
		this.manager = manager;

		lastInfo = getNow();
		startingTime = getNow();

		room.onMatchStart ~= &started;
	}

	int requiredSkipUsers()
	{
		auto numUsers = room.slots[].count!(a => a != OsuRoom.Settings.Player.init) - 1;
		return cast(int) round(numUsers * requiredSkips);
	}

	bool canStillSkipMidMap()
	{
		return (getNow() - startingTime) < currentMapDuration / 2;
	}

	void reset(bool freshHost)
	{
		skippers.length = 0;
		didSkip = false;
		if (freshHost)
		{
			isRepeated = false;
			requiredSkips = 1;
		}
	}

	void started()
	{
		startingTime = getNow();
		skippedMapIDs.length = 0;
	}

	bool shouldSkip(string map)
	{
		if (skippedMapIDs.canFind(map))
			return true;
		else
			return false;
	}

	bool doSkip(string user, string map)
	{
		if (skippers.canFind(user))
			return shouldSkip(map);
		if (manager.probablyPlaying && !canStillSkipMidMap)
		{
			if (getNow() - lastInfo < 10.seconds)
				return false;
			lastInfo = getNow();
			room.sendMessage("Map is already too far progressed to skip it now.");
			return false;
		}
		if (didSkip)
			return false;
		try
		{
			GameUser.findByUsername(user).voteSkip(skippers.length == 0);
		}
		catch (Exception e)
		{
			logError("Error in !skip by %s: %s", user, e);
			room.sendMessage(user ~ ": yeah I can understand this decision.");
		}
		skippers ~= user;
		if (skippers.length >= requiredSkipUsers)
		{
			didSkip = true;
			skippedMapIDs ~= map;
			return true;
		}
		else
		{
			room.sendMessage("Map skip progress: " ~ .text(skippers.length, "/", requiredSkipUsers));
			return false;
		}
	}
}

abstract class MapSelector
{
	abstract void load(ManagedRoom room);
	abstract void handleCommand(Message msg);
	abstract void nextSelect();
	abstract void gotSkipped();
	abstract void toggleTempHost(string host);
	abstract bool isValidMap(string map);
	abstract void evaluateMap(MapInfo map);

	void invalidStart()
	{
	}

	void onUserHost(string user)
	{
	}

	void onUserJoin(string user, ubyte slot)
	{
	}

	void onUserLeave(string user)
	{
	}

	void onClosed()
	{
	}

	void onFirstJoin()
	{
	}
}

abstract class GenericPlaylistSelector : MapSelector
{
	ManagedRoom room;
	string[] users;
	ManagedWaitingState managedWaitingState;

	override void load(ManagedRoom room)
	{
		this.room = room;

		managedWaitingState = new ManagedWaitingState(room);
	}

	abstract size_t mapsLeft() @property;
	abstract size_t mapsTotal() @property;
	abstract void resetMaps();

	override void handleCommand(Message msg)
	{
		if (msg.message == "!queue")
		{
			room.room.sendMessage("I still got " ~ mapsLeft.to!string
					~ " different maps to play before repeating " ~ mapsTotal.to!string ~ " maps.");
		}
	}

	override void gotSkipped()
	{
		nextSelect();
	}

	override void toggleTempHost(string host)
	{
		if (room.maintenanceHost.length)
		{
			room.maintenanceHost = null;
			room.room.clearhost();
		}
		else
		{
			room.maintenanceHost = host;
			room.room.host = host;
		}
	}

	override bool isValidMap(string map)
	{
		return true;
	}

	override void evaluateMap(MapInfo map)
	{
		room.skipper.currentMapDuration = map.playtime;
		room.skipper.requiredSkips = 0.5;
		room.room.sendMessage(
				"Use !skip to skip this map. Skips: 0/" ~ room.skipper.requiredSkipUsers.to!string);
	}

	override void onFirstJoin()
	{
		resetMaps();
		room.switchState(managedWaitingState);
	}

	override void onUserLeave(string user)
	{
		size_t numPlayers;
		foreach (slot; room.room.slots)
		{
			if (slot.name.length && slot.name != user)
				numPlayers++;
		}
		if (numPlayers == 0)
		{
			room.abortStart();
			room.switchState(room.emptyState);
		}
	}
}

class PlaylistSelector : GenericPlaylistSelector
{
	MapInfo[] possibleMaps, allPossibleMaps;

	this(AutoHostSettings settings)
	{
		foreach (user; settings.userPlaylistSettings.mappers)
		{
			auto maps = queryMaps(["u" : user]);
			string[] errors;
			foreach (pair; maps.sort!"a.setID < b.setID"
					.filter!(a => settings.evaluateMapSkip(a, errors) <= 0.05)
					.chunkBy!"a.setID")
			{
				auto group = pair[1].array;
				if (group.length == 0)
					continue;
				final switch (settings.userPlaylistSettings.mapDiff)
				{
				case AutoHostSettings.MapDiffPick.all:
					possibleMaps ~= group;
					break;
				case AutoHostSettings.MapDiffPick.random:
					possibleMaps ~= group.randomSample(group.length,
							group.length).take(settings.userPlaylistSettings.mapDiffMaxNum).array;
					break;
				case AutoHostSettings.MapDiffPick.lowest:
					possibleMaps ~= group.sort!"a.difficultyRating < b.difficultyRating".take(
							settings.userPlaylistSettings.mapDiffMaxNum).array;
					break;
				case AutoHostSettings.MapDiffPick.highest:
					possibleMaps ~= group.sort!"a.difficultyRating > b.difficultyRating".take(
							settings.userPlaylistSettings.mapDiffMaxNum).array;
					break;
				}
			}
		}
		possibleMaps.randomShuffle;
		allPossibleMaps = possibleMaps;

		if (allPossibleMaps.length == 0)
			throw new Exception("Did not find any maps in playlist mode");
	}

	override void resetMaps()
	{
		allPossibleMaps.randomShuffle;
		possibleMaps = allPossibleMaps;
	}

	override size_t mapsLeft() @property
	{
		return possibleMaps.length;
	}

	override size_t mapsTotal() @property
	{
		return allPossibleMaps.length;
	}

	override void nextSelect()
	{
		if (possibleMaps.empty)
			resetMaps();
		auto map = possibleMaps.front;
		possibleMaps.popFront;
		room.room.map = map.beatmapID.to!string;
		evaluateMap(map);
		room.switchState(managedWaitingState);
	}
}

class LitePlaylistSelector : GenericPlaylistSelector
{
	MapWithInfo[] possibleMaps, allPossibleMaps;

	this(AutoHostSettings settings)
	{
		possibleMaps = settings.genericPlaylistSettings.maps[];
		possibleMaps.randomShuffle;
		allPossibleMaps = possibleMaps;

		if (allPossibleMaps.length == 0)
			throw new Exception("Did not find any maps in playlist mode");
	}

	override void resetMaps()
	{
		allPossibleMaps.randomShuffle;
		possibleMaps = allPossibleMaps;
	}

	override size_t mapsLeft() @property
	{
		return possibleMaps.length;
	}

	override size_t mapsTotal() @property
	{
		return allPossibleMaps.length;
	}

	override void nextSelect()
	{
		if (possibleMaps.empty)
			resetMaps();
		auto map = possibleMaps.front;
		possibleMaps.popFront;
		room.room.map = map.map.to!string;
		room.room.mods = map.mods;
		if (map.comment.length)
			room.sendMessage(map.comment);
		evaluateMap(queryMap(map.map));
		room.switchState(managedWaitingState);
	}
}

class HostRotator : MapSelector
{
	ManagedRoom room;

	string[] hostOrder, failedHostPassing, badRepSkipped;
	size_t currentHostIndex;
	size_t actualCurrentHost;

	SelectMapState selectState;
	WaitingState waitingState;

	this(string[] hostOrder, size_t currentHostIndex)
	{
		this.hostOrder = hostOrder;
		this.currentHostIndex = currentHostIndex;
	}

	override void load(ManagedRoom room)
	{
		this.room = room;

		selectState = new SelectMapState(room, this);
		waitingState = new WaitingState(room, this);
	}

	/// !hostqueue, !start (as host)
	override void handleCommand(Message msg)
	{
		string text = msg.message.strip;
		if (text == "!hostqueue")
		{
			string suffix;
			auto index = hostOrder.countUntil(msg.sender);
			if (index == -1)
				suffix = ". " ~ msg.sender ~ ": you are currently not eligible for host. Try rejoining.";
			else if (index != currentHostIndex)
			{
				auto waits = (cast(long) index - cast(long) currentHostIndex + hostOrder.length) % hostOrder
					.length;
				suffix = ". " ~ msg.sender ~ ": you are host after at most " ~ waits.to!string ~ " other user" ~ (waits == 1
						? ". :)" : "s. :)");
			}
			if (hostOrder.length == 0)
				room.sendMessage("No next hosts." ~ suffix);
			else if (hostOrder.length <= 2)
				room.sendMessage("Next host is " ~ hostOrder.cycle.drop(currentHostIndex + 1).front ~ suffix);
			else if (hostOrder.length > 2)
				room.sendMessage("Next hosts are " ~ hostOrder.cycle.drop(currentHostIndex + 1)
						.take(min(5, hostOrder.length - 1)).join(", ") ~ suffix);
		}
		else if (text == "!start" && msg.sender == currentHost && !room.probablyPlaying)
		{
			room.startGame(room.settings.manualStartDuration);
		}
	}

	override void nextSelect()
	{
		nextHost();
	}

	override void invalidStart()
	{
		string user = currentHost;
		nextHost();
		if (user.length)
		{
			try
			{
				GameUser.findByUsername(user).didInvalidStart();
				room.sendMessage(user ~ ": tried to start an invalid map, aborting and skipping host.");
			}
			catch (Exception e)
			{
				logError("Error in foul onMatchStart by %s: %s", user, e);
				room.sendMessage(
						user ~ ": tried to be a smartass and started anyway. Aborting and skipping host.");
			}
		}
	}

	override bool isValidMap(string map)
	{
		return selectState.isValidMap(map);
	}

	string nextHost(bool deleteCurrent = false, bool message = true)
	{
		room.switchState(selectState);
		if (deleteCurrent && currentHostIndex < hostOrder.length)
			hostOrder = hostOrder[0 .. currentHostIndex] ~ hostOrder[currentHostIndex + 1 .. $];
		if (hostOrder.length == 0)
		{
			// everyone left
			//room.close();
			onRoomEmpty();
			room.skipper.skippedMapIDs.length = 0;
			return null;
		}
		else
		{
			if (!deleteCurrent)
				currentHostIndex = (currentHostIndex + 1) % hostOrder.length;
			else if (currentHostIndex >= hostOrder.length)
				currentHostIndex = 0;
			auto user = hostOrder[currentHostIndex];
			try
			{
				if (!room.room.hasPlayer(user))
				{
					failedHostPassing ~= user;
					return nextHost(true, message);
				}
				try
				{
					if (!GameUser.findByUsername(user).shouldGiveHost)
					{
						badRepSkipped ~= user;
						return nextHost(true, message);
					}
				}
				catch (Exception e)
				{
					logError("Error in nextHost: %s", e);
					room.sendMessage(user ~ ": Lucky you!");
				}
				serializeToDB();
				string suffix = ".";
				if (failedHostPassing.length)
				{
					if (failedHostPassing.length > 2)
						suffix ~= " " ~ failedHostPassing.length.to!string ~ " previous users left.";
					else
						suffix ~= " " ~ failedHostPassing.join(", ") ~ " left and got skipped.";
					failedHostPassing.length = 0;
				}
				if (badRepSkipped.length)
				{
					if (badRepSkipped.length > 2)
					{
						suffix ~= " Skipping " ~ badRepSkipped.length.to!string
							~ " users because of current bad reputation.";
					}
					else
					{
						suffix ~= " Skipping " ~ badRepSkipped.join(
								", ") ~ " because of current bad reputation.";
					}
					badRepSkipped.length = 0;
				}
				room.room.host = user;
				actualCurrentHost = currentHostIndex;
				if (message)
				{
					if (hostOrder.length == 0)
						room.sendMessage("No next hosts" ~ suffix);
					else if (hostOrder.length <= 2)
						room.sendMessage("Next host is " ~ hostOrder.cycle.drop(currentHostIndex + 1)
								.front ~ suffix);
					else if (hostOrder.length > 2)
						room.sendMessage("Next hosts are " ~ hostOrder.cycle.drop(currentHostIndex + 1)
								.take(min(3, hostOrder.length - 1)).join(", ") ~ suffix);
				}
				return user;
			}
			catch (Exception e)
			{
				logError("Failed to pass host: %s", e);
				failedHostPassing ~= user;
				return nextHost(true, message);
			}
		}
	}

	string currentHost() @property
	{
		if (currentHostIndex >= hostOrder.length)
			return null;
		return hostOrder[currentHostIndex];
	}

	override void gotSkipped()
	{
		string host = currentHost;
		if (room.probablyPlaying)
		{
			room.room.abortMatch();
			room.room.onMatchEnd.emit();
		}
		else
		{
			string suffix;
			if (room.skipper.isRepeated)
			{
				nextHost();
				suffix = "picking next host";
			}
			else
			{
				room.skipper.isRepeated = true;
				room.switchState(selectState);
				suffix = "please pick another map";
			}
			try
			{
				GameUser.findByUsername(host).gotSkipped();
				room.sendMessage(host ~ ": your map got skipped, " ~ suffix ~ ".");
			}
			catch (Exception e)
			{
				logError("Error in %s's map getting skipped: %s", host, e);
				room.sendMessage(host ~ ": that's a crappy map, " ~ suffix ~ ".");
			}
		}
	}

	override void toggleTempHost(string user)
	{
		if (room.maintenanceHost.length)
		{
			room.maintenanceHost = null;
			room.room.host = currentHost;
		}
		else
		{
			room.maintenanceHost = user;
			room.room.host = user;
		}
	}

	void onRoomEmpty()
	{
		failedHostPassing.length = 0;
		badRepSkipped.length = 0;
		room.playHistory.length = 0;
		room.switchState(room.emptyState);
	}

	override void onUserHost(string user)
	{
		if (actualCurrentHost == currentHostIndex && user != currentHost)
		{
			string s = nextHost(false, false);
			if (s != user)
				room.sendMessage("Host passed elsewhere, passing where it was supposed to go next");
		}
	}

	override void onUserJoin(string user, ubyte slot)
	{
		if (hostOrder.canFind(user))
			return;
		if (hostOrder.length)
		{
			// insert before current host
			hostOrder = hostOrder[0 .. currentHostIndex] ~ user ~ hostOrder[currentHostIndex .. $];
			currentHostIndex++;
			actualCurrentHost++;
		}
		else
		{
			hostOrder = [user]; // first join!
			nextHost();
			room.switchState(waitingState);
		}
	}

	override void onUserLeave(string user)
	{
		if (user == currentHost)
		{
			if (!room.probablyPlaying)
				nextHost(true);
			else
			{
				try
				{
					auto obj = GameUser.findByUsername(user);
					obj.didLeaveAsHost();
					room.sendMessage(user ~ " has left as host, this is sad.");
				}
				catch (Exception e)
				{
					room.sendMessage(
							user ~ " has left as host, what a pleb. Laugh at them if they join again.");
				}
			}
		}
	}

	override void onClosed()
	{
		hostOrder = [];
		currentHostIndex = 0;
	}

	override void onFirstJoin()
	{
		room.switchState(waitingState);
	}

	override void evaluateMap(MapInfo map)
	{
		string[] errors;
		auto skippiness = room.settings.evaluateMapSkip(map, errors);
		if (skippiness > 0)
		{
			room.skipper.requiredSkips = 1 - skippiness;
			room.room.sendMessage("Map does not fully qualify to room settings: " ~ errors.join(
					" ") ~ " Use !skip to skip this map. Skips: 0/"
					~ room.skipper.requiredSkipUsers.to!string);
		}
		else
			room.skipper.requiredSkips = 1;
	}

	void serializeToDB()
	{
		auto persist = PersistentAutoHostRoom.findById(room.persistID);
		persist.hostOrder = hostOrder;
		persist.currentHost = currentHostIndex;
		persist.settings = room.settings;
		persist.save();
	}
}

class ManagedRoom
{
	static struct StateTimer
	{
		State state;
		bool local;
		Timer timer;
	}

	IngameState ingameState;
	EmptyState emptyState;
	UnknownState unknownState;

	State currentState;
	StateTimer stateTimer;

	SysTime lastInfo;
	SysTime startingGameTimerFinished;
	bool startingGame;
	bool probablyPlaying;
	string expectedTitle;
	string maintenanceHost;

	BeatmapInfo currentMapInfo;
	BeatmapInfo[] playHistory;

	OsuRoom room;
	AutoHostSettings settings;

	NewUserGreeter greeter;
	MapSkipper skipper;
	MapSelector selector;

	BsonObjectID persistID;

	this(OsuRoom room, AutoHostSettings settings, string expectedTitle,
			MapSelector selector, BsonObjectID persistID)
	{
		this.persistID = persistID;

		this.selector = selector;
		this.selector.load(this);

		ingameState = new IngameState(this);
		emptyState = new EmptyState(this);
		unknownState = new UnknownState(this);

		currentState = unknownState;

		this.expectedTitle = expectedTitle;

		lastInfo = getNow();
		startingGameTimerFinished = getNow();

		greeter.load(room);
		skipper.load(room, this);

		this.room = room;
		this.settings = settings;

		playHistory.reserve(100);

		room.onBeatmapPending ~= &this.onBeatmapPending;
		room.onBeatmapChanged ~= &this.onBeatmapChanged;
		room.onCountdownFinished ~= &this.onCountdownFinished;
		room.onUserHost ~= &this.onUserHost;
		room.onUserJoin ~= &this.onUserJoin;
		room.onUserLeave ~= &this.onUserLeave;
		room.onPlayersReady ~= &this.onPlayersReady;
		room.onMatchStart ~= &this.onMatchStart;
		room.onMatchEnd ~= &this.onMatchEnd;
		room.onMessage ~= &this.onMessage;
		room.onClosed ~= &this.onClosed;

		switchState(unknownState);
	}

	void pushHistory(BeatmapInfo info)
	{
		if (playHistory.length > 100)
		{
			foreach (i; 0 .. playHistory.length - 1)
				playHistory[i] = playHistory[i + 1];
			playHistory[$ - 1] = info;
		}
		else
			playHistory ~= info;
	}

	MapInfo selectMap(BeatmapInfo mapInfo)
	{
		currentMapInfo = mapInfo;
		try
		{
			auto map = queryMap(currentMapInfo.id);
			if (map == MapInfo.init)
				throw new Exception("Could not find beatmap.");
			skipper.currentMapDuration = map.playtime;
			selector.evaluateMap(map);
			return map;
		}
		catch (Exception e)
		{
			room.sendMessage("Error: Map could not be looked up. " ~ e.msg);
			skipper.requiredSkips = 0.8;
			skipper.currentMapDuration = 5.minutes;
			return MapInfo.init;
		}
	}

	void setStateTimer(Duration d, bool announce)
	{
		if (stateTimer.local)
			stateTimer.timer.stop();
		stateTimer.state = currentState;
		if (announce)
		{
			stateTimer.local = false;
			room.setTimer(d);
		}
		else
		{
			stateTimer.local = true;
			stateTimer.timer = setTimer(d, { this.onCountdownFinished(); });
		}
	}

	void abortStart(bool aggressive = true)
	{
		if (!startingGame)
			return;
		bool close = false;
		if (getNow() + 2.seconds > startingGameTimerFinished
				&& getNow() < startingGameTimerFinished + 2.seconds)
			close = true;
		room.abortTimer();
		startingGame = false;
		if (aggressive && close)
		{
			runTask({ sleep(3.seconds); room.abortMatch(); onMatchEnd(); });
		}
	}

	void abortStateTimer()
	{
		if (stateTimer.state is null || startingGame)
			return;
		stateTimer.state = null;
		if (stateTimer.local)
			stateTimer.timer.stop();
		else
			room.abortTimer();
	}

	void startGame(Duration after)
	{
		if (startingGame && getNow() + after > startingGameTimerFinished)
			return;
		if (startingGame)
			abortStateTimer();
		room.start(after);
		startingGame = true;
		startingGameTimerFinished = getNow() + after;
	}

	void switchState(State state)
	{
		abortStateTimer();
		stateTimer = StateTimer.init;
		currentState.leave();
		currentState = state;
		currentState.enter();
	}

	bool wasRecentlyPlayed(string id, int lookbehind = -1)
	{
		if (lookbehind == -1)
			lookbehind = settings.recentlyPlayedLength;
		int count = 0;
		foreach_reverse (other; playHistory)
		{
			if (other.id == id)
				return true;
			if (count++ >= lookbehind)
				break;
		}
		return false;
	}

	void sendMessage(string msg, HighPriority highPriority = HighPriority.no)
	{
		room.sendMessage(msg, highPriority);
	}

	void nextSelect()
	{
		skipper.reset(true);
		selector.nextSelect();
	}

	void firstJoin()
	{
		selector.onFirstJoin();
	}

	//
	// ===== EVENTS =====
	//

	void onMessage(Message msg)
	{
		logDebug("Message: %s", msg);
		string text = msg.message.strip;
		if (text == "!info")
		{
			if (getNow() - lastInfo < 10.seconds)
				return;
			lastInfo = getNow();
			room.sendMessage("Auto Host & AFK prevention, keeping lobbying as vanilla as possible. Source Code: https://gitlab.com/WebFreak001/OsuAutoHost DM me !info for more");
		}
		else if (text == "!me")
		{
			try
			{
				auto user = GameUser.findByUsername(msg.sender);
				room.sendMessage(
						msg.sender ~ ": You left as host " ~ user.hostLeaves.length.to!string ~ " times and got penalized for it "
						~ user.numHostLeavePenalties.to!string
						~ " times. You have joined auto host " ~ user.joins.to!string ~ " times.");
				room.sendMessage(
						msg.sender ~ ": So far " ~ user.picksGotSkipped.to!string ~ " of your map picks got skipped. You have played "
						~ user.playStarts.to!string ~ " times and finished "
						~ user.playFinishes.to!string ~ " times out of these.");
			}
			catch (Exception e)
			{
				logError("Error in !me by %s: %s", msg.sender, e);
				room.sendMessage(msg.sender ~ ": I don't know anything about you :(");
			}
		}
		else if (text == "!skip")
		{
			bool shouldSkip = skipper.doSkip(msg.sender, currentMapInfo.id);
			if (shouldSkip)
				selector.gotSkipped();
		}
		else if (text == "!r")
		{
			if (getNow() - lastInfo < 10.seconds)
				return;
			lastInfo = getNow();
			room.sendMessage(
					msg.sender
					~ ": This room doesn't require any special commands, just play as usual. See !info");
		}
		else if (text == "!temphost" && msg.sender == settings.creator)
		{
			selector.toggleTempHost(msg.sender);
		}
		else
			selector.handleCommand(msg);
	}

	void onCountdownFinished()
	{
		if (stateTimer.state == currentState && !startingGame)
		{
			stateTimer = StateTimer.init;
			currentState.onStateTimerFinished();
		}
	}

	void onBeatmapChanged(BeatmapInfo beatmap)
	{
		currentState.onBeatmapChanged(beatmap);
	}

	void onBeatmapPending()
	{
		currentState.onBeatmapPending();
	}

	void onPlayersReady()
	{
		currentState.onPlayersReady();
	}

	void onMatchStart()
	{
		if (!selector.isValidMap(currentMapInfo.id))
		{
			selector.invalidStart();
			room.abortMatch();
		}
		else if (!room.slots.length)
		{
			room.abortMatch();
			room.sendMessage("Currently having no users. Aborting match");
			nextSelect();
		}
		else
		{
			bool error;
			size_t numPlayers;
			foreach (slot; room.slots)
			{
				try
				{
					if (slot.name.length)
					{
						GameUser.findByUsername(slot.name).didStart();
						numPlayers++;
					}
				}
				catch (Exception e)
				{
					error = true;
					logError("Error onMatchStart: %s", e);
				}
			}
			if (error)
				room.sendMessage("Have fun playing :)");
			if (numPlayers == 0)
			{
				room.abortMatch();
				switchState(emptyState);
			}
			else
			{
				probablyPlaying = true;
				currentState.onMatchStart();
				switchState(ingameState);
			}
		}
	}

	void onMatchEnd()
	{
		bool error;
		foreach (slot; room.slots)
		{
			try
			{
				if (slot.name.length)
					GameUser.findByUsername(slot.name).didFinish();
			}
			catch (Exception e)
			{
				error = true;
				logError("Error onMatchEnd: %s", e);
			}
		}
		if (error)
			room.sendMessage("Congrats for making it until the end \\o/");

		startingGame = false;
		probablyPlaying = false;
		currentState.onMatchEnd();
		nextSelect();
	}

	void onUserHost(string user)
	{
		if (user == maintenanceHost)
			return;
		selector.onUserHost(user);
	}

	void onUserJoin(string user, ubyte slot, Team)
	{
		currentState.onUserJoin(user, slot);
		selector.onUserJoin(user, slot);
	}

	void onUserLeave(string user)
	{
		selector.onUserLeave(user);
	}

	void onClosed()
	{
		selector.onClosed();

		if (!settings.autoReopen)
			return;

		room = banchoConnection.createRoom(expectedTitle);

		auto persist = PersistentAutoHostRoom.findById(persistID);
		persist.mpID = "#mp_" ~ room.room;
		persist.settings = settings;
		persist.save();
		room.password = settings.password;
		foreach (user; settings.startInvite)
			room.invite(user);
		runTask({ room.size = settings.slots; room.mods = [Mod.FreeMod]; });
		switchState(emptyState);
		playHistory.length = 0;
	}
}

abstract class State
{
	void enter();
	void leave();

	void onMatchEnd()
	{
	}

	void onBeatmapChanged(BeatmapInfo beatmap)
	{
	}

	void onBeatmapPending()
	{
	}

	void onStateTimerFinished()
	{
	}

	void onPlayersReady()
	{
	}

	void onMatchStart()
	{
	}

	void onUserJoin(string user, ubyte slot)
	{
	}
}

class SelectMapState : State
{
	ManagedRoom room;
	HostRotator rotator;
	bool warningTimeout;
	this(ManagedRoom room, HostRotator rotator)
	{
		this.room = room;
		this.rotator = rotator;
	}

	override void enter()
	{
		startIdleTimer();
	}

	override void leave()
	{

	}

	void startIdleTimer()
	{
		warningTimeout = true;
		room.setStateTimer(room.settings.selectWarningTimeout, false);
	}

	bool isValidMap(string id)
	{
		return !room.skipper.shouldSkip(id) && !room.wasRecentlyPlayed(id);
	}

	override void onBeatmapChanged(BeatmapInfo beatmap)
	{
		room.currentMapInfo = beatmap;
		room.abortStateTimer();
		string title = beatmap.name;
		if (room.currentMapInfo.id != beatmap.id)
		{
			room.skipper.reset(false);
			room.selectMap(beatmap);
		}
		auto shouldSkip = room.skipper.shouldSkip(beatmap.id);
		if (room.wasRecentlyPlayed(beatmap.id))
		{
			room.room.sendMessage(
					rotator.currentHost ~ ": Please pick another map! " ~ title ~ " was recently played.");
			startIdleTimer();
		}
		else if (shouldSkip)
		{
			room.room.sendMessage(
					rotator.currentHost ~ ": Please pick another map! " ~ title ~ " was skipped.");
			startIdleTimer();
		}
		else
		{
			room.switchState(rotator.waitingState);
		}
	}

	override void onBeatmapPending()
	{
		warningTimeout = true;
		if (room.stateTimer.local)
		{
			room.abortStateTimer();
			room.setStateTimer(room.settings.selectWarningTimeout, false);
		}
	}

	override void onStateTimerFinished()
	{
		if (warningTimeout)
		{
			room.setStateTimer(room.settings.selectIdleChangeTimeout, true);
			warningTimeout = false;
			room.room.sendMessage(
					rotator.currentHost ~ ": please pick a map or next host will be picked!");
		}
		else
		{
			rotator.nextHost();
		}
	}
}

class WaitingState : State
{
	ManagedRoom room;
	HostRotator rotator;
	this(ManagedRoom room, HostRotator rotator)
	{
		this.room = room;
		this.rotator = rotator;
	}

	override void enter()
	{
		room.startGame(room.settings.startGameDuration);
	}

	override void leave()
	{
		room.abortStart(false);
	}

	override void onBeatmapChanged(BeatmapInfo beatmap)
	{
		room.switchState(rotator.selectState);
		rotator.selectState.onBeatmapChanged(beatmap);
	}

	override void onBeatmapPending()
	{
		room.switchState(rotator.selectState);
		rotator.selectState.onBeatmapPending();
	}

	override void onPlayersReady()
	{
		room.startGame(room.settings.allReadyStartDuration);
	}

	override void onMatchStart()
	{
		room.pushHistory(room.currentMapInfo);
	}
}

class ManagedWaitingState : State
{
	ManagedRoom room;
	this(ManagedRoom room)
	{
		this.room = room;
	}

	override void enter()
	{
		room.startGame(room.settings.startGameDuration);
	}

	override void leave()
	{
		room.abortStart(false);
	}

	override void onPlayersReady()
	{
		room.startGame(room.settings.allReadyStartDuration);
	}

	override void onMatchStart()
	{
		room.pushHistory(room.currentMapInfo);
	}
}

class IngameState : State
{
	ManagedRoom room;
	this(ManagedRoom room)
	{
		this.room = room;
	}

	override void onBeatmapChanged(BeatmapInfo beatmap)
	{
		room.room.sendMessage("How did you manage to change the beatmap in-game? :thinking:");
	}

	override void enter()
	{
		if (room.settings.enforcePassword)
			room.room.password = room.settings.password;
		room.abortStart(false);
		if (room.settings.enforceTitle)
		{
			auto info = room.room.settings;
			if (room.expectedTitle.length && info.name.strip != room.expectedTitle.strip)
				room.room.sendMessage("Please rename room name to: " ~ room.expectedTitle);
		}
	}

	override void leave()
	{

	}
}

class EmptyState : State
{
	ManagedRoom room;
	this(ManagedRoom room)
	{
		this.room = room;
	}

	override void enter()
	{
	}

	override void leave()
	{
	}

	override void onUserJoin(string user, ubyte slot)
	{
		room.firstJoin();
	}
}

class UnknownState : State
{
	ManagedRoom room;
	this(ManagedRoom room)
	{
		this.room = room;
	}

	override void enter()
	{
	}

	override void leave()
	{
	}

	override void onBeatmapChanged(BeatmapInfo beatmap)
	{
		if (auto rotator = cast(HostRotator) room.selector)
		{
			room.switchState(rotator.selectState);
			rotator.selectState.onBeatmapChanged(beatmap);
		}
	}

	override void onBeatmapPending()
	{
		if (auto rotator = cast(HostRotator) room.selector)
		{
			room.switchState(rotator.selectState);
			rotator.selectState.onBeatmapPending();
		}
	}

	override void onPlayersReady()
	{
		if (auto playlist = cast(PlaylistSelector) room.selector)
		{
			room.switchState(playlist.managedWaitingState);
			playlist.managedWaitingState.onPlayersReady();
		}
	}
}

struct DMRateLimit
{
	string user;
	SysTime lastInfo;
}

DMRateLimit[] ratelimits;

void handlePrivateMessage(Message msg)
{
	size_t i = size_t.max;
	foreach (n, info; ratelimits)
		if (info.user == msg.sender)
		{
			i = n;
			break;
		}
	if (i == size_t.max)
		ratelimits ~= DMRateLimit(msg.sender, getNow());
	else if (getNow() - ratelimits[i].lastInfo < 10.seconds)
		return;
	else
		ratelimits[i].lastInfo = getNow();
	if (msg.message == "!info" || msg.message == "!help" || msg.message == "!mp")
	{
		banchoConnection.sendMessage(msg.sender,
				"Source Code: https://gitlab.com/WebFreak001/OsuAutoHost");
		sleep(1.seconds);
		banchoConnection.sendMessage(msg.sender,
				"Commands: !skip - if map doesn't fit criteria, skip a map before (or in-game) to avoid playing it");
		sleep(1.seconds);
		banchoConnection.sendMessage(msg.sender,
				"* !me - shows you your play information in this hosted instance of Auto Host");
		sleep(1.seconds);
		banchoConnection.sendMessage(msg.sender, "* !hostqueue - In a Mini Host Rotate room this shows you when you get host (most users in the list could have left, so it's not actually that number all the time)");
		sleep(1.seconds);
		banchoConnection.sendMessage(msg.sender,
				"* !start - In a Mini Host Rotate room this allows you to quick start the game as host.");
	}
}
